// 1. Create an activity.js file on where to write and save the solution for the activity.
// 2. Find users with letter s in their first name or d in their last name.
// - Use the $or operator.

// - Show only the firstName and lastName fields and hide the _id field.
// 3. Find users who are from the HR department and their age is greater than or equal to 70.
// - Use the $and operator
// 4. Find users with the letter e in their first name and has an age of less than or equal to 30.
// - Use the $and, $regex and $lte operators



db.user.insert({
    firstname: "Jane",
    lastname:"Doe"
});

db.user.insert({
    firstname: "Stephen",
    lastname:"Hawkings",
    age: 70,
    contact:[],
    department: "HR"
});

db.user.insert({
    firstname: "Neil",
    lastname:"Armstrong",
    age: 82,
    contact:[]*2,
    courses:[]*3,
    department: "HR"
});

db.rooms.find().pretty();
